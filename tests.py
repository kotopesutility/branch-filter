import unittest
from compare_branches import compare_branches as cb

class CompareBranchTest(unittest.TestCase):

    def test_diff_branches(self):
        """
        Check function diff_branches from compare_branches.
        """
        dic_1 = [{'name':'pckg0', 'version':1}, {'name':'pckg2', 'version':2}]
        dic_2 = [{'name':'pckg1', 'version':1}, {'name':'pckg2', 'version':1}]
        result = cb.diff_branches(dic_1, dic_2)
        answer = [{'name':'pckg0', 'version':1}]
        self.assertEqual(result, answer) 

    def test_filter_intersection(self):
        """
        Check function filter_intersection from compare_branches.
        """
        dic_1 = [{'name':'pckg0', 'version':1}, {'name':'pckg2', 'version':2}]
        dic_2 = [{'name':'pckg1', 'version':1}, {'name':'pckg2', 'version':1}]
        result = cb.filter_intersection(dic_1, dic_2, 'version')
        answer = [{'name':'pckg2', 'version':2}]
        self.assertEqual(result, answer) 

    def test_archs_list(self):
        """
        Check function get_branch_archs.
        """
        archs = set(['aarch64', 'armh', 'i586', 'noarch', 'ppc64le', 'x86_64', 'x86_64-i586'])
        branch = cb.get_branch_packages('sisyphus')
        result = cb.get_branch_archs(branch)
        self.assertEqual(result, archs)

if __name__ == '__main__':
    unittest.main()
