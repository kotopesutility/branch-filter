#! /usr/bin/python3
import compare_branches
import sys
import json
import argparse

def arguements_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--branches',
            nargs = 2,
            help = 'provide branches names',
            required = True)
    parser.add_argument('-o',
            type = str,
            metavar = 'output.json',
            default = 'result.json',
            help = 'provide json-file for output')
    parser.add_argument('-k', '--key',
            type = str,
            metavar = 'key_value',
            default = 'version',
            help = 'provide key to compare branches(For example: version)')
    return parser.parse_args()

def main():
    args = arguements_parsing()
    
    result = compare_branches.compare(
            branch_1 = args.branches[0], branch_2 = args.branches[1],
            key = args.key)
    with open(args.o, 'w') as w:
        json.dump(result, w)

if __name__ == '__main__':
    main()
