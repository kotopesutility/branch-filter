# compare_packages
Filter packages from different branches.

## compare_packages_cli.py
Utility to use this module, returns output in a json file.

### usage:
compare_branches_cli.py [-h] -b BRANCHES BRANCHES [-o output.json] [-k key_value]

### options:
-  -h, --help            show this help message and exit
-  -b, --branches BRANCHES BRANCHES
                        provide branches names
-  -o output.json        provide json-file for output. Default value is 'result.json'
-  -k, --key key_value       provide key to compare branches(For example: version). Default value is 'version'

---

## Requirements:
* python3(requests)
* python3(argparse)
* python3(json)
