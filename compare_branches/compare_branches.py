import requests
import sys
import json
from .constants import API_BRANCH_PKGS_URL

def return_readable_error(status, message):
    """
    Returns error in a readable format.

    Keyword arguements:
    status -- status (error) code
    message -- dictionary with error message
    """
    answer = f'Error\n'
    if status:
        answer += f'Status code:{status}\n'

    for keys, value in message.items():
        answer += f'{keys}:{value}\n'
    return answer

def get_branch_packages(branch):
    """
    Gets packages for branch using API.

    Keyword arguements:
    branch -- branch name
    """
    url = f'{API_BRANCH_PKGS_URL}/{branch}'
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()['packages']
    else:
        raise Exception(return_readable_error(response.status_code, response.json()))

def diff_branches(branch_1, branch_2):
    """
    Get the difference for packages names between two branches.

    Keyword arguements:
    branch_1 -- name of the first branch
    branch_2 -- name of the second branch
    """
    bset_1 = set(x['name'] for x in branch_1)
    bset_2 = set(x['name'] for x in branch_2)
    bset_diff = bset_1.difference(bset_2)

    answer = [obj for obj in branch_1 if obj['name'] in bset_diff]
    return answer

def filter_intersection(branch_1, branch_2, key):
    """
    Gets the intersection of 2 branches and
    choose those packages from the first branch
    if their key value is bigger.

    Keyword arguements:
    branch_1 -- name of the first branch
    branch_2 -- name of the second branch
    key -- key value for which branches packages will be compared
    """
    bset_1 = set(obj['name'] for obj in branch_1)
    bset_2 = set(obj['name'] for obj in branch_2)
    bset_intersection = bset_1.intersection(bset_2)

    new_branch_1 = [obj for obj in branch_1 if obj['name'] in bset_intersection]
    new_branch_2 = [obj for obj in branch_2 if obj['name'] in bset_intersection]

    answer = [new_branch_1[i] for i in range(len(new_branch_1)) if new_branch_1[i][key] > new_branch_2[i][key]]
    return answer

def get_branch_archs(branch):
    return set(obj['arch'] for obj in branch)

def compare(branch_1, branch_2, key = 'version'):
    """
    Compares 2 branches for packages names and checks
    if the first branch has packages with key value bigger
    than the second one.

    Keyword arguements:
    branch_1 -- name of the first branch
    branch_2 -- name of the second branch
    key -- key value for which branches packages will be compared
    """
    branch_packages_1 = get_branch_packages(branch_1)
    archs_1 = get_branch_archs(branch_packages_1)

    branch_packages_2 = get_branch_packages(branch_2)
    archs_2 = get_branch_archs(branch_packages_2)

    result = {}
    for arch in sorted(archs_1.intersection(archs_2)):
        packages_1 =  [obj for obj in branch_packages_1 if obj['arch'] == arch] 
        packages_2 =  [obj for obj in branch_packages_2 if obj['arch'] == arch] 

        only_in_2 = diff_branches(packages_2, packages_1)
        only_in_1 = diff_branches(packages_1, packages_2)
        filtered_branch = filter_intersection(packages_1, packages_2, key) 

        result[arch] = {f'only_in_{branch_2}': only_in_2,
                f'only_in_{branch_1}': only_in_1,
                f'{key}_larger_in_{branch_1}': filtered_branch}
    return result
